import debug from 'debug'

let log = debug('document service')
log.enabled = true

function sleep(ms: number) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export class DocumentService {
  private db: Record<number, string> = {}

  private queue: number[] = []

  private processingId?: number

  private lastTime = 0

  private interval = 10 * 1000

  enqueue(id: number) {
    if (id in this.db) {
      let summary = this.db[id]
      return { id, summary }
    }
    if (this.processingId == id) {
      return
    }
    if (this.queue.includes(id)) {
      return
    }
    this.queue.push(id)
    this.checkQueue()
    return { id, summary: null }
  }

  private async checkQueue() {
    // use loop, or use setTimeout to call checkQueue again in the finally block
    for (;;) {
      if (this.processingId) {
        log('queue is busy, check later')
        return
      }
      let id = this.queue.shift()
      if (!id) {
        log('queue is clear')
        return
      }
      log('queue is idle, pick document:', id)
      try {
        this.processingId = id
        let summary = await this.summarize(id)
        this.db[id] = summary
      } finally {
        delete this.processingId
      }
    }
  }

  private async summarize(id: number) {
    let now = Date.now()
    if (now - this.lastTime < this.interval) {
      let coolDownTime = this.lastTime + this.interval - now
      log('wait for cool down, delta:', coolDownTime)
      await sleep(coolDownTime)
    }
    this.lastTime = Date.now()
    log('start to summarize', { id })
    let n = 5
    for (let i = 1; i <= n; i++) {
      await sleep(1000)
      log(`[${i}/${n}]:`, { id })
    }
    log('done summarize', { id })
    return 'Done at ' + new Date().toLocaleString()
  }
}
