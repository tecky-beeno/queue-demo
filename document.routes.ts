import express from 'express'
import { DocumentService } from './document.service'

let router = express.Router()

export default router

let service = new DocumentService()

router.get('/documents/:id/summarize', async (req, res) => {
  let id = +req.params.id
  if (!id) {
    res.json('invalid id in req.params')
    return
  }
  let document = await service.enqueue(id)
  res.json({ document })
})
